use rayon::iter::ParallelBridge;

use {
    bytes::Buf as _,
    color_eyre::Result,
    ftp::FtpStream,
    rayon::{iter::ParallelIterator as _, slice::ParallelSlice as _},
    rust_decimal::Decimal,
    rust_decimal_macros::dec,
    tap::{Conv as _, Tap as _},
    tracing::{error, info},
};

fn parse_centimeters(mut buffer: &[u8]) -> Decimal {
    buffer
        .get_i32()
        .conv::<Decimal>()
        .tap_mut(|d| d.set_scale(2).unwrap())
}

fn parse_degrees(mut contents: &[u8]) -> Decimal {
    contents
        .get_i32()
        .conv::<Decimal>()
        .tap_mut(|d| d.set_scale(6).unwrap())
}

fn parse_record(contents: &[u8]) -> Vec<[Decimal; 3]> {
    let latitude = parse_degrees(&contents[336..]);
    let longitude = parse_degrees(&contents[340..]);
    let geoid_radius = parse_centimeters(&contents[612..]);
    let delta_geoid = parse_centimeters(&contents[640..]);
    let delta_latitude = parse_degrees(&contents[768..]);
    let delta_longitude = parse_degrees(&contents[772..]);

    (0..20)
        .filter(|shot| {
            let offset = 4 * (162 + shot);
            0 < (&contents[offset..]).get_i32()
        })
        .filter(|shot| {
            let offset = 2 * (192 + shot);
            (&contents[offset..]).get_i16() == 1
        })
        .map(|shot| {
            let offset = 4 * (shot + 12);
            (
                shot,
                parse_centimeters(&contents[offset..]),
                (shot.conv::<Decimal>() - dec!(10.5)) / dec!(20),
            )
        })
        .map(|(shot, planetary_radius, x)| {
            (
                shot,
                planetary_radius, // TODO crossover correction
                x,
                latitude + x * delta_latitude, // TODO parallax correction
                longitude + x * delta_longitude, // TODO parallax correction
                planetary_radius - geoid_radius + x * delta_geoid,
            )
        })
        .map(|(_, _, _, latitude, longitude, topography)| [latitude, longitude, topography])
        .collect()
}

fn parse_pedr_file(mut contents: &[u8]) -> Vec<[Decimal; 3]> {
    // Discard header
    contents.advance(7760);

    assert_eq!(contents.len() % 776, 0);

    contents
        .par_chunks_exact(776)
        .flat_map(|record| parse_record(record))
        .collect()
}

fn install_tracing() {
    use {
        tracing::Level,
        tracing_error::ErrorLayer,
        tracing_subscriber::prelude::*,
        tracing_subscriber::{fmt, EnvFilter},
    };

    let fmt_layer = fmt::layer().pretty().with_target(false);
    let filter_layer = EnvFilter::try_from_default_env()
        .or_else(|_| EnvFilter::try_new(Level::INFO.as_str()))
        .unwrap();

    tracing_subscriber::registry()
        .with(filter_layer)
        .with(fmt_layer)
        .with(ErrorLayer::default())
        .init();
}

fn main() -> Result<()> {
    install_tracing();
    color_eyre::install()?;

    info!("connecting to PDS ftp server");
    let mut stream = FtpStream::connect("pds-geosciences.wustl.edu:21")?;
    stream.login("anonymous", "")?;
    info!("login successful");

    info!("changing to data directory");
    stream.cwd("mgs/mgs-m-mola-3-pedr-l1a-v1/mgsl_21xx/data")?;

    let record_count = stream
        .nlst(None)?
        .into_iter()
        .filter_map(|dir| list_data_files(&mut stream, dir))
        .flatten()
        .take(2)
        .collect::<Vec<_>>()
        .into_iter()
        .filter_map(|path| download_pedr_file(&mut stream, path))
        .par_bridge()
        .flat_map(|(_, contents)| parse_pedr_file(&contents))
        .count();

    dbg!(record_count);

    Ok(())
}

#[tracing::instrument(skip(stream))]
fn list_data_files(stream: &mut FtpStream, dir: String) -> Option<impl Iterator<Item = String>> {
    match stream.nlst(Some(&dir)) {
        Ok(files) => Some(
            files
                .into_iter()
                .map(move |file| format!("{}/{}", dir, file)),
        ),
        Err(error) => {
            error!(%dir, %error, "could not list files for directory");
            None
        }
    }
}

#[tracing::instrument(skip(stream))]
fn download_pedr_file(stream: &mut FtpStream, path: String) -> Option<(String, Vec<u8>)> {
    match stream.simple_retr(&path) {
        Ok(cursor) => Some((path, cursor.into_inner())),
        Err(error) => {
            error!(%error, "could not download PEDR file");
            None
        }
    }
}
